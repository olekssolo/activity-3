public class User {
    private String username;
    private String password;
    private UserType usertype;

    public User(String username, String password, UserType usertype {
        this.username = username;
        this.password = password;
        this.usertype = usertype;
    }

    public String getUser() {
        return this.username;
    }
    public String getPass() {
        return this.password;
    }
    public UserType getType() {
        return this.usertype;
    }
}
